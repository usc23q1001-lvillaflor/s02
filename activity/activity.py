# 2
name = "Liam"
age = 22
occupation = "student"
movie = "Avengers: Endgame"
rating = "90.5"


print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %")

# 3
num1 = 5 
num2 = 10
num3 = 15

print(num1 * num2)
print(num1 < num3)
num2 += num3
print(num2)