# Comments
# Comments in Python are done using the "#" symbol
# ctrl/cmd + / - comments in Python - one line comment

age = 18
middle_initial = "C"
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

full_name = "John Doe"
secret_code = "Pa$$word"

num_of_days = 365
pi_approx = 3.1416
complex_num = 1 + 5j

isLearning = True
isDifficult = False

print("My name is " + full_name)
print("My age is " + str(age))

# Typecasting
print(int(3.5))
print(int("9861"))

# F-strings
print(f"My age is {age}")
print(f"Hi, my name is {full_name} and my age is {age}")

# Operation
# Arithmetic (math operations)

print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4)
print( 2 ** 6)

# Assignment Operators
num1 = 3
num1 += 4
print(num1)
num1 -= 4
print(num1)
num1 *= 4
print(num1)
num1 /= 4
print(num1)
num1 **= 4
print(num1)

# Comparison Operator
print(1 == 1)
print(5 > 10)
print(5 < 10)
print(1 <= 1)
print(2 >= 3)
print(1 != 1)

# Logical Operator
print(True and False)
print(True or False)
print(not False)